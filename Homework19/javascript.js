задача 1
function divided (a,b) {
    if (a % b === 0) {
        console.log("Делится: " + (a / b));
    }
    else {
        console.log( "Делится с остатком: " + (a % b));
    }
}
divided(10, 4)

//Задача 2
var height;
var sym;
var tmp;

function DrawTriangle (height, sym) {
    tmp = sym;
    for (let i = 0; i < height; i++) {
        console.log (tmp);
        tmp += sym;
    }
}
DrawTriangle(7,'^');

//задача 3
for (let i = 0; i <= 100; i++) {
    if (i % 2 > 0) {
        console.log (i);
    }
}

//Задача 4
var n = 1000;
while (n >= 50) {
    n /= 2;
    console.log(n);
}
