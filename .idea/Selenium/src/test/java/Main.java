
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;

public class Main {
    WebDriver driver = new ChromeDriver();

    @BeforeClass
    public static void beforeClass() {
        final String path = String.format("%s/bin/chromedriver.exe",
                System.getProperty("user.dir"));
        System.setProperty("webdriver.chrome.driver", path);
    }

    @Test
    public void firstTest() {
        driver.get("https://user-data.hillel.it/html/registration.html");
        driver.manage().window().maximize();
        driver.findElement(By.className("registration")).click();
        driver.findElement(By.id("first_name")).sendKeys("Denis");
        driver.findElement(By.id("last_name")).sendKeys("Bunakov");
        driver.findElement(By.id("field_work_phone")).sendKeys("1234567");
        driver.findElement(By.id("field_phone")).sendKeys("380502130047");
        driver.findElement(By.id("field_email")).sendKeys("bunakov5@gmail.com");
        driver.findElement(By.id("field_password")).sendKeys("1Step2Close");
        driver.findElement(By.id("male")).click();
        driver.findElement(By.id("position")).click();
        driver.findElement(By.id("position")).sendKeys(Keys.ARROW_DOWN);
        driver.findElement(By.id("position")).sendKeys(Keys.ARROW_DOWN);
        driver.findElement(By.id("position")).click();
        driver.findElement(By.id("button_account")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        {
            WebDriverWait wait = new WebDriverWait(driver, 5);
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = driver.switchTo().alert();
            alert.accept();
        }
        driver.findElement(By.className("authorization")).click();
        driver.findElement(By.id("email")).sendKeys("bunakov5@gmail.com");
        driver.findElement(By.id("password")).sendKeys("1Step2Close");
        driver.findElement(By.className("login_button")).click();
        driver.findElement(By.linkText("Employees")).click();
        driver.findElement(By.id("first_name")).sendKeys("Denis");
        driver.findElement(By.id("position")).click();
        driver.findElement(By.id("position")).sendKeys(Keys.ARROW_DOWN);
        driver.findElement(By.id("search")).click();
    }
}
