package object;

public class User {
    private String firstName;
    private String lastname;
    private String email;
    private String mobilePhone;
    private String workPhone;
    private String password;

    public User(String firstName, String lastname, String email, String mobilePhone, String workPhone, String password) {
        this.firstName = firstName;
        setLastname(lastname);
        setEmail(email);
        this.mobilePhone = mobilePhone;
        this.workPhone = workPhone;
        setPassword(password);
    }

    public User(String email, String password, String surname) {
        setEmail(email);
        setPassword(password);
        setLastname(lastname);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        char [] charArray = email.toCharArray();
        boolean check = false;
        for (char c : charArray) {
            if (c == '@') {
                this.email = email;
                check = true;
                String resultSentence = getFirstName() + "'s Email is - " + email;
                System.out.println(resultSentence);
            }
        }
        if (!check) {
            System.out.println("Email should contain @");
        }
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if (password.length() >= 8 && password.length() <= 16) {
            String resultSentence = getFirstName() + "'s password is - " + password;
            System.out.println(resultSentence);
            this.password = password;
        }
        else {
            System.out.println("Password should be minimum 8, max 16 symbols");
        }
    }

    public void setLastname(String lastname) {
        if (lastname.length() >= 2 && lastname.length() <= 10) {
            System.out.println("Lastname is correct for user " + getFirstName());
            this.lastname = lastname;
        }
        else {
            System.out.println("Lastname should be minimum 2, max 10 symbols");
        }
    }
}
