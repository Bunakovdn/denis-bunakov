package object;

public class Offspring extends User {

        private double salary;
        private int experience;

        public Offspring(String firstName, String lastname, String email, String mobilePhone, String workPhone,
                         String password, double salary, int experience) {
            super (firstName, lastname, email, mobilePhone, workPhone, password);
            this.salary = salary;
            this.experience = experience;
        }

        public double getSalary() {
            return salary;
        }

        public void setSalary(double salary) {
            this.salary = salary;
        }

        public int getExperience() {
            return experience;
        }

        public void setExperience(int experience) {
            this.experience = experience;
        }

        public static double changeSalary(double salary, int experience) {
            if (experience < 24) {
                salary = salary + salary * 0.05;
            } else if (experience >= 24 && experience <= 60) {
                salary = salary + salary * 0.1;
            } else if (experience > 60) {
                salary = salary + salary * 0.15;
            }

            return salary;
        }

    }

