package object;

public class Main1 {

    public static void main(String[] args) {

        User user1 = new User("Denis", "Bunakov", "Bunakov@gmail.com", "+380502130043", "1234567", "Pas word1");
        User user2 = new User("Bunakov.d.n@gmail.com", "Password1", "Bunakov");
        Offspring offspring = new Offspring("Andrey", "Konstantinov", "Konstantinov@gmail.com", "+380502253252", "Password2", "Password1",
                20000, 12);

        System.out.println(user1.getEmail());
        System.out.println(
                "Salary = " + offspring.changeSalary(offspring.getSalary(), offspring.getExperience()));
    }
}

